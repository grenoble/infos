.. index::
   pair: Organisation; CNT-F
   pair: Organisation; CNT38
   pair: Organisation; Extinction rebellion Grenoble
   ! Organisations

.. _orgas_grenoble:

=========================================
**Organisations sur Grenoble**
=========================================

.. _cnt38:

**CNT-38 Anarcho-syndicalisme et Syndicalisme Révolutionnaire**
=================================================================

- http://www.cnt-f.org/ul38/
- https://www.instagram.com/cnt_38/


Congrès CNT-F jusqu'en 2021
==================================

- https://cntf.frama.io/congres-confederaux/


Extinction rebellion Grenoble
================================

- https://linktr.ee/xrgrenoble


**Solidarités Grenoble**
=========================================

- https://www.solidarites-grenoble.fr/
- https://www.solidarites-grenoble.fr/958-annuaire-des-acteurs-de-l-action-sociale-a-grenoble.htm


UCL Grenoble
==============

- https://linktr.ee/uclgrenoble
- https://www.instagram.com/ucl_grenoble/
- https://unioncommunistelibertaire.org/
