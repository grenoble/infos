.. index::
   ! Agendas

.. raw:: html

   <a rel="me" href="https://kolektiva.social/@grenobleluttes"></a>


.. _agendas_grenoble:

=========================================
**Agendas sur Grenoble**
=========================================

- https://rstockm.github.io/mastowall/?hashtags=grenoble&server=https://framapiaf.org


Agenda grenoble-luttes
===========================

- https://openagenda.com/evenements-grenoble-luttes

**Musée de la Résistance et de la Déportation de l'Isère**
==========================================================

- https://musees.isere.fr/liste-agendas?musee=15


L'agenda de Grenoble
======================

- https://www.gremag.fr/12-agenda.htm


|ici_grenoble| ici-grenoble
=================================

- https://www.ici-grenoble.org/agenda
- https://www.ici-grenoble.org/

|tamis| Le tamis
=====================

- https://www.le-tamis.info/evenements

|cric| cric-grenoble
=======================

- https://cric-grenoble.info/spip.php?page=agenda
- https://cric-grenoble.info/infos-locales


38.demosphere
==============================================

- https://38.demosphere.net/


.. _extinction_rebellion:

|rebellion| Agenda extinction rebellion Grenoble
=====================================================

- https://extinctionrebellion.fr/agenda/
- https://www.ici-grenoble.org/structure/extinction-rebellion-grenoble
- https://linktr.ee/xrgrenoble

.. figure:: images/xr_grenoble.png
   :align: center

   https://mobilizon.chapril.org/@xr_grenoble@mobilizon.extinctionrebellion.fr, https://extinctionrebellion.fr/agenda/


|alternatiba| Agenda alternatiba
====================================

- https://grenoble.alternatiba.eu/calendrier/
- https://framaforms.org/inscriptions-printemps-des-formations-alternatiba-xr-1681292117


Antigone
===========

- Agenda : https://www.bibliothequeantigone.org/?page_id=1069&ai1ec=
- Agenda du mois: https://www.bibliothequeantigone.org/?page_id=1069&ai1ec\=action~month


Agenda Attac
===============

- https://france.attac.org/se-mobiliser/retraites-pour-le-droit-a-une-retraite-digne-et-heureuse/article/on-ne-les-lache-pas-la-carte-des-mobilisations
- https://100joursdezbeul.fr/


Agendas du logiciel libre Grenoble
=====================================

- https://fr.wikipedia.org/wiki/Free_Software_Foundation#Le_logiciel_libre
- https://www.agendadulibre.org/ (agenda national)
- https://meta.wikimedia.org/wiki/Events_calendar (agenda national)
- https://wiki.openstreetmap.org/wiki/Grenoble_groupe_local/Agenda (:ref:`agenda des événements d'Openstreetmap Grenoble <logiciel_grenoble:openstreetmap>`)
- http://www.agendadulibre.org/tags/openstreetmap
- https://www.logre.eu/events/ (:ref:`agenda des événements de Logre <logiciel_grenoble:logre>`)

Human talks Grenoble
------------------------

- https://humantalks.com/cities/grenoble

La turbine (Fabrique collaborative  de services numériques d'intérêt général)
==============================================================================

- https://turbine.coop/
- https://turbine.coop/programmation/


|medics| Comptes rendus de l'observatoire des Street-médics
=============================================================

- https://obs-medics.org/
- https://obs-medics.org/sabonner/
- https://obs-medics.org/bilan/


https://lucse.gr/agenda
==========================

- https://lucse.gr/agenda/
